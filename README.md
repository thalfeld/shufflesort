##Shuffle & Sort

Demo: https://kexe.dk/thalfeld/shufflesort/


####Install
`npm install`
####Develop
`npm run start`
####Build
`npm run start`

(or yarn equivalent)

**Author: Thomas Halfeld**
LinkedIn: https://www.linkedin.com/in/thomas-halfeld/;
