import "../styles/styles.scss";

//collect element
const list = document.getElementById("list");
const sortButton = document.getElementById("sortButton");
const shuffleButton = document.getElementById("shuffleButton");

const initialList = [1, 2, 3, 4, 5, 6, 7, 8, 9];

//rendering methods
function createItem(label) {
    const item = document.createElement("li");
    item.innerHTML = `<div class="sorting-app-item-content">${label}</div>`;
    item.className = `color-${label} sorting-app-item`;
    return item;
}

function renderList(numbers) {
    list.innerHTML = "";
    numbers.forEach((n) => {
        const item = createItem(n);
        list.append(item);
    });
}

//Attaching eventlistener for controls
sortButton.addEventListener("click", () => {
    renderList(initialList);
});

shuffleButton.addEventListener("click", () => {
    const list = [].concat(initialList).sort(() => {
        return Math.random() > 0.5 ? 1 : -1;
    });
    renderList(list);
});

// render initial list
renderList(initialList);
